from selenium import webdriver
from selenium.common.exceptions import WebDriverException
import os
from time import sleep
from selenium.webdriver.chrome.options import Options
import json

def get_files(env_settings):

    file_paths = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(env_settings['input_report']):
        for file in f:
            if '.html' in file:
                file_paths.append(os.path.join(r, file))

    return file_paths


def scrappy(files):

    first_execution = True
    template_list = test_status_final = ""
    
    try:
        chrome_options = Options()  
        chrome_options.add_argument("--headless")  
        chrome_options.binary_location = env_settings['chrome_binary']
        driver = webdriver.Chrome(executable_path=env_settings['chrome_driver'], chrome_options=chrome_options)
        
        for f in files:
        
            driver.delete_all_cookies()
            file_name = 'file://' + f
            driver.get(file_name)
            driver.execute_script("return document.readyState=='complete'")
            sleep(3)
            driver.find_element_by_xpath('//*[@id="detail-tabs"]/li[1]/a').click()
            sleep(1)
            driver.find_element_by_id('radio-all').click()
            sleep(1)
            driver.find_element_by_class_name('details-col-header').click()

            test_name = test_status = ""

            list_test_status = driver.find_elements_by_class_name('details-col-status')
            for ts in list_test_status:
                    if str(ts.text).__contains__('FAIL'):
                        test_status += "<div style='background-color:red'>" + ts.text + "</div>"
                    else:
                        test_status += "<div style='background-color:green'>" + ts.text + "</div>"
            
            test_status = test_status.replace('Status', '')
            test_status_final += "<td>" + os.path.basename(f).replace('.html', '') + test_status + "</td>"

            if first_execution:
                list_test_name = driver.find_elements_by_class_name('details-col-name')

                for tn in list_test_name:
                    try:
                        test_name += "<div>" + "TC-" + str(tn.text).split("TC-")[1][0:50] + "...</div>"
                    except:
                        pass

                test_name = test_name.replace('Name', '')
                test_name_final = "<td>TC - Name" + test_name + "</td>"
                
                first_execution = False

        template_list = test_status_final + test_name_final

    finally:
        driver.quit()

    return template_list


def output_report(template_list, env_settings):

    html_template = """
    <!DOCTYPE html>
    <html lang="en" dir="ltr">
        <head>
            <meta charset="utf-8">
            <title>REPORT GENERATOR</title>
        </head>
        <body>
            <table border=1>
            <tr> """ + template_list + """
            </tr>
            </table>
        </body>
    </html>
    """

    f = open(env_settings['output_report'] + "result.html", "w")
    f.write(html_template)
    f.close()

if __name__ == "__main__":

    with open('properties.json') as json_data_file:
        env_settings = json.load(json_data_file)

    print("Retrieving files ...\n")
    files = get_files(env_settings)
    print("Getting data from report ...\n")
    template = scrappy(files)
    output_report(template, env_settings)
    print("Done!\n")